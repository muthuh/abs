# abs
#### **A Bash Skel - whatever it is you are planning on doing - do it via the Bash.**

The idea is to have a simple and unified way of deploying Bash scripts - whatever their purpose might be.

**Description**

All executable scripts under the 'tasks' directory will be run in an alphabetical order.

**Capabilities**

logging, conditionals, unified and 'prettyfied' output, pick-up where you left of.

**Word of caution**

This collection of Bash gibberish is provided with the intent of being useful and with the best of intentions - sadly it's actual usefulness and accuracy is yet to be verified and cannot be guaranteed.

**Usage**

`./abe`

**Further notes**

Conditional executable scripts (if any) should be placed in the 'prerequisites' directory - each must succeed in order for the 'tasks' to be executed. An example conditional script is provided - the tasks will -not- run with the superuser privileges - try it!

Logging functionality is provided via the 'logit' function - example usage:

`logit ! 'A message to be logged.'`

Note: The exclamation mark in the above indicates the 'log level' - it's entirely optional, if ommited the messages are logged at the default 'info' [i] level.

The 'state' of the task execution is being tracked - it is possible to continue where you last left of on the subsequent run.

If the second line of the prerequisite/task script is a comment starting with '# ' the task's/prerequisite's name (printed in the output) is set to the remainder of that line, otherwise the file name is used. In case this isn't clear, the included example tasks better illustrate this point.

Investigate the two provided example 'tasks' - and replace them with your own to fit your purpose.

**Final note**

Feedback is very much welcome and appreciated - please share any kind of experiences and suggestions - it will drive further development of the project.
